#!/usr/bin/env perl6

use v6.c;

unit module Local::Tildes::Challenge;

my %translations =
	 0 => "oh",
	10 => "ten",
	11 => "eleven",
	12 => "twelve",
	15 => "fifteen",
	20 => "twenty",
	30 => "thirty",
	40 => "fourty",
	50 => "fifty",
;

for 1..9 {
	%translations{$_} = $_.Str.uninames.head.split(' ').tail.lc
}

sub convert (
	Str:D $time,
	--> Str
) is export {
	my ($hours, $minutes) = $time.split(':');
	my $stringified = "{convert-hours($hours)} {convert-minutes($minutes)} {am-pm($hours)}".subst(/\s+/, ' ', :g);
	"It's $stringified";
}

sub am-pm (
	Str:D $hours,
	--> Str
) {
	$hours.Int < 12 ?? "am" !! "pm";
}

sub convert-hours (
	Str:D $hours,
	--> Str
) {
	my $hours-int = $hours.Int % 12;

	return %translations{12} if $hours-int == 0;
	
	%translations{$hours-int};
}

sub convert-minutes (
	Str:D $minutes,
	--> Str
) {
	my $spoken = '';

	return '' if $minutes eq '00';
	return %translations{$minutes} if (%translations ∋ $minutes.Str);

	for $minutes.comb {
		$spoken ~= %translations{$_} ~ ' ';
	}

	$spoken.trim;
}
