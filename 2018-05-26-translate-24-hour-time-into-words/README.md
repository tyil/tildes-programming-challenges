# Programming Challenge: Translate 24-hour time into words

This is an adapted version of Talking Clock from
[/r/DailyProgrammer](https://www.reddit.com/r/dailyprogrammer/).

The point of this thread is to post your code for solving the task. Other will
comment with feedback and new ideas. Post what language (and version, if
relevant) your code is written in.

## Task description

Your task is to translate a 24-hour formatted time to words.

## Input description

An hour between 00 and 23 followed by a colon followed by a minute between 0
and 59.

## Output description

The time expressed in words, in 12-hour format followed by "am" or "pm".

## Sample input

```
00:00
01:30
12:05
14:01
```

## Sample output

```
It's twelve am
It's one thirty am
It's twelve oh five pm
It's two oh one pm
```
