#!/usr/bin/env perl6

use v6.c;

use Local::Tildes::Challenge;
use Test;

is convert('00:00'), "It's twelve am";
is convert('01:30'), "It's one thirty am";
is convert('12:05'), "It's twelve oh five pm";
is convert('14:01'), "It's two oh one pm";
