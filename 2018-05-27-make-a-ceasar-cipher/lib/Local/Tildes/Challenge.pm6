#! /usr/bin/env false

use v6.c;

#| Specify an exported sub which takes and returns the format specified in the
#| challenge.
sub caesar (
	Str:D $input,
	--> Str
) is export {
	# Use a regex to extract the cleartext and the shift value from the input
	if ($input ~~ / ^ (.*) " " (\d+) $ /) {
		return shift-string(~$0, +$1);
	}
}

sub caesar-reverse (
	Str:D $input,
	--> Str
) is export {
	# Use a regex to extract the cleartext and the shift value from the input
	if ($input ~~ / ^ (.*) " " (\d+) $ /) {
		return shift-string(~$0, -+$1);
	}
}

#| Shift all characters in a given C<Str> a number of C<$shift> times.
sub shift-string (
	Str:D $clear,
	Int:D $shift,
	--> Str
) {
	$clear.comb.map({ shift-char($_, $shift) }).join;
}

#| Shift a single character a number of C<$shift> times.
multi sub shift-char (
	Str:D $character is copy where *.chars == 1,
	Int:D $shift where * > 0,
	--> Str
) {
	for ^($shift % 26) {
		$character = shift-forward($character);
	}

	$character;
}

multi sub shift-char (
	Str:D $character is copy where *.chars == 1,
	Int:D $shift where * < 0,
	--> Str
) {
	for ^(-$shift % 26) {
		$character = shift-backward($character);
	}

	$character;
}

#| Shift a single character one time
sub shift-forward (
	Str:D $character where *.chars == 1,
	--> Str
) {
	return 'a' if $character eq 'z';
	return 'A' if $character eq 'Z';

	$character.succ;
}

sub shift-backward (
	Str:D $character where *.chars == 1,
	--> Str
) {
	return 'z' if $character eq 'a';
	return 'Z' if $character eq 'A';

	$character.pred;
}
