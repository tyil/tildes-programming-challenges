# Programming Challenge: Make a Ceasar Cipher

The point of this thread is to post your code for solving the task. Other will
comment with feedback and new ideas. Post what language (and version, if
relevant) your code is written in.
Have fun!

## Task description

Your task is to make a caesar cipher that takes a word and an integer as
arguments.
[An article](https://en.wikipedia.org/wiki/Caesar_cipher) explaining what the
cipher does.

## Input description

A word followed by a space and an integer.

## Output description

The ciphered word.

## Sample input

```
A 1
Caesar 5
Tildes 25
```

## Sample output

```
B
Hfjxfw
Shkcdr
```

## Bonus 1

Make the cipher work backwards.

### Sample input

```
B 1
Hfjxfw 5
Shkcdr 25
```

### Sample output

```
A
Caesar
Tildes
```

## Bonus 2

Make the cipher handle special characters.

### Sample input

```
A_ 1
Cae?sar 5
Til!des 25
```

### Sample output

```
B_
Hfj?xfw
Shk!cdr
```
