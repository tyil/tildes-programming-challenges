#! /usr/bin/env perl6

use v6.c;

use Local::Tildes::Challenge;
use Test;

plan 3;

is caesar-reverse("B 1"), "A";
is caesar-reverse("Hfjxfw 5"), "Caesar";
is caesar-reverse("Shkcdr 25"), "Tildes";
