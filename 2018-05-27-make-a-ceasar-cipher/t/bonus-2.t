#! /usr/bin/env perl6

use v6.c;

use Local::Tildes::Challenge;
use Test;

plan 3;

is caesar("A_ 1"), "B_";
is caesar("Cae?sar 5"), "Hfj?xfw";
is caesar("Til!des 25"), "Shk!cdr";
