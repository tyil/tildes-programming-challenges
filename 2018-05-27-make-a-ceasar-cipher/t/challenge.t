#! /usr/bin/env perl6

use v6.c;

use Local::Tildes::Challenge;
use Test;

plan 3;

is caesar("A 1"), "B";
is caesar("Caesar 5"), "Hfjxfw";
is caesar("Tildes 25"), "Shkcdr";
